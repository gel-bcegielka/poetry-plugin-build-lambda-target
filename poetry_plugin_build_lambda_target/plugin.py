import logging
import os
import shlex
import subprocess
import zipfile
from dataclasses import dataclass, field
from pathlib import Path
from typing import Any

from cleo.helpers import option
from poetry.console.application import Application
from poetry.console.commands.build import BuildCommand
from poetry.core.masonry.builders.wheel import WheelBuilder
from poetry.core.masonry.utils.helpers import distribution_name
from poetry.plugins.application_plugin import ApplicationPlugin

logger = logging.getLogger(__name__)


@dataclass(frozen=True)
class ArtifactsConfig:
    target_dir: Path
    wheel_filename: str

    requirements_txt: str = "requirements.txt"
    lambda_build_artifact_name: str = "package"
    lambda_build_module_name: str = "lambda_function"
    lambda_build_handler_name: str = "lambda_handler"

    lambda_build_module_name_src: str = "lambda_function"
    lambda_build_handler_name_src: str = "lambda_handler"

    target_requirements_txt: str = field(init=False)
    target_wheel_filename: str = field(init=False)
    target_lambda_build_artifact_name: str = field(init=False)
    target_lambda_build_artifact_zip: str = field(init=False)
    lambda_entrypoint: str = field(init=False)
    target_lambda_entrypoint: str = field(init=False)

    def __post_init__(self):
        object.__setattr__(
            self,
            "target_requirements_txt",
            self.target_dir / self.requirements_txt,
        )
        object.__setattr__(
            self, "target_wheel_filename", self.target_dir / self.wheel_filename
        )
        object.__setattr__(
            self,
            "target_lambda_build_artifact_name",
            self.target_dir / self.lambda_build_artifact_name,
        )
        object.__setattr__(
            self,
            "lambda_entrypoint",
            f"{self.lambda_build_artifact_name}/{self.lambda_build_module_name}.py",
        )
        object.__setattr__(
            self,
            "target_lambda_entrypoint",
            self.target_dir / self.lambda_entrypoint,
        )
        object.__setattr__(
            self,
            "target_lambda_build_artifact_zip",
            f"{self.target_lambda_build_artifact_name}.zip",
        )


class CustomBuildTargetCommand(BuildCommand):
    lambda_build_format_name = "lambda"
    pyproject_section_name = "poetry-plugin-build-lambda-target"

    pyproject_opt_autobuild = "autobuild"
    pyproject_opt_lambda_build_module_name_src = "lambda_build_module_name_src"
    pyproject_opt_lambda_build_handler_name_src = "lambda_build_handler_name_src"

    name = "build"
    options = list(
        map(
            lambda o: (
                option(o.name, o.shortcut, o.description + " &lambda", flag=False)
                if o.name == "format"
                else o
            ),
            BuildCommand.options,
        )
    )

    def handle(self) -> int:
        self.line("Custom build command with support for AWS lambda target")
        return super().handle()
        # return 0

    def _build(
        self,
        fmt: str,
        executable: str | Path | None = None,
        *,
        target_dir: Path | None = None,
    ) -> None:
        if fmt != self.lambda_build_format_name and fmt != "all":
            return super()._build(fmt, executable, target_dir=target_dir)

        plugin_conf = self.poetry.pyproject.data["tool"].get(
            self.pyproject_section_name, {}
        )
        wb = WheelBuilder(self.poetry, executable=executable)

        artifacts_config_params = {
            "target_dir": target_dir,
            "wheel_filename": wb.wheel_filename,
        }

        if plugin_conf.get(self.pyproject_opt_lambda_build_module_name_src):
            artifacts_config_params["lambda_build_module_name_src"] = plugin_conf.get(self.pyproject_opt_lambda_build_module_name_src)
        if plugin_conf.get(self.pyproject_opt_lambda_build_handler_name_src):
            artifacts_config_params["lambda_build_handler_name_src"] = plugin_conf.get(self.pyproject_opt_lambda_build_handler_name_src)

        artifacts_config = ArtifactsConfig(**artifacts_config_params)

        if fmt == self.lambda_build_format_name:
            # lambda depends on wheel build so building it
            wb.build(target_dir)

            self._build_lambda(executable, artifacts_config)
        elif fmt == "all":
            super()._build(fmt, executable, target_dir=target_dir)

            if plugin_conf.get(self.pyproject_opt_autobuild):
                self._build_lambda(executable, artifacts_config)

    def _build_lambda(self, executable, artifacts_config):
        self.line("Building lambda")

        # export requirements.txt from poetry.lock
        self.call(
            "export",
            f"-f requirements.txt --output {artifacts_config.target_requirements_txt}  --without-hashes",
        )
        self.line(f"Generated: {artifacts_config.target_requirements_txt}")

        # pip install requirements & wheel
        pip = shlex.split(
            f"pip install -r {artifacts_config.target_requirements_txt} --upgrade --only-binary :all: --platform manylinux2014_x86_64 --target {artifacts_config.target_lambda_build_artifact_name} {artifacts_config.target_wheel_filename}",
        )
        subprocess.check_call([executable.as_posix(), "-m", *pip])

        # add lambda handler to top dir
        self.add_lambda_entrypoint(
            artifacts_config.target_lambda_entrypoint,
            artifacts_config.lambda_build_module_name_src,
            artifacts_config.lambda_build_handler_name_src,
            artifacts_config.lambda_build_handler_name,
        )
        self.line(f"Generated: {artifacts_config.target_lambda_entrypoint}")

        self.create_zip_artifact(
            artifacts_config.target_lambda_build_artifact_name,
            artifacts_config.target_lambda_build_artifact_zip,
        )
        self.line(f"Generated: {artifacts_config.target_lambda_build_artifact_zip}")

    def add_lambda_entrypoint(
        self, entrypoint, lambda_build_module_name_src, lambda_build_handler_name_src, lambda_build_handler_name
    ):
        package = distribution_name(self.poetry._package.name)
        content = f"from {package}.{lambda_build_module_name_src} import {lambda_build_handler_name_src} as {lambda_build_handler_name}\n"
        with open(entrypoint, "w") as fh:
            fh.write(content)

    def create_zip_artifact(self, artifact_source_dir, artifact_zip):
        with zipfile.ZipFile(f"{artifact_zip}", "w", zipfile.ZIP_DEFLATED) as zipfh:
            for root, _, files in os.walk(artifact_source_dir):
                if Path(root).name == "__pycache__":
                    continue
                for file in files:
                    zipfh.write(
                        Path(root) / file,
                        (Path(root) / file).relative_to(artifact_source_dir),
                    )


def factory():
    command = CustomBuildTargetCommand()
    return command


class BuildLambdaTargetPlugin(ApplicationPlugin):
    def activate(self, application: Application, *args: Any, **kwargs: Any) -> None:
        # build command already exists, needs to be removed before reregistering
        del application.command_loader._factories["build"]
        application.command_loader.register_factory("build", factory)
